<div id="views_slideshow_swiper_teaser_section_<?php print $variables['vss_id']; ?>" class="<?php print $classes; ?>">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <?php print $rendered_rows; ?>
    </div>
  </div>
</div>
