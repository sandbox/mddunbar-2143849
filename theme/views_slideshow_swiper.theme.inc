<?php

/**
 * @file
 * Theme & preprocess functions for the Views Slideshow: swiper module.
 */

/**
 * Views Slideshow: Theme the main frame wrapper.
 *
 * @ingroup themeable
 */

function _views_slideshow_swiper_preprocess_views_slideshow_swiper_main_frame(&$vars) {
  $settings = $vars['settings'];
  $rows = $vars['rows'];
  $view = $vars['view'];
  $vss_id = $vars['vss_id'];

  // Cast the strings into int or bool as necessary.
  $new_settings = array();
  foreach ($settings as $key => $value) {
    if (is_string($value)) {
      $value = str_replace("\n", ' ', $value);

      $value = trim($value);

      if (is_numeric($value)) {
        $value = (int)$value;
      }
      elseif (strtolower($value) == 'true') {
        $value = TRUE;
      }
      elseif (strtolower($value) == 'false') {
        $value = FALSE;
      }
    }

    $new_settings[$key] = $value;
  }

  // Don't load javascript unless libraries module is present.
  if (module_exists('libraries')) {
    // Load jQuery Swiper
    if ($swiper_path = _views_slideshow_swiper_library_path()) {
      drupal_add_js($swiper_path);
    }

    // Load our swiper js
    $module_path = drupal_get_path('module', 'views_slideshow_swiper');
    drupal_add_js($module_path . '/js/views_slideshow_swiper.js');
  }

  // Load our swiper css
  drupal_add_css($module_path . '/views_slideshow_swiper.css', 'file');

  drupal_add_js(array('viewsSlideshowSwiper' => $new_settings), 'setting');

  // Add the slideshow elements.
  $vars['classes_array'][] = 'views_slideshow_swiper_teaser_section';

  $styles = '';
  if (isset($view->display_handler->display->display_options['style_options']['views_slideshow_swiper'])) {
    $styles = $view->display_handler->display->display_options['style_options']['views_slideshow_swiper'];
  }

  $styles_default = '';
  if (isset($view->display['default']->display_options['style_options']['views_slideshow_swiper'])) {
    $styles_default = $view->display['default']->display_options['style_options']['views_slideshow_swiper'];
  }

  // Retrive the number of items per frame
  if (isset($styles['items_per_slide']) && $styles['items_per_slide'] > 0) {
    $items_per_slide = $styles['items_per_slide'];
  }
  elseif (isset($styles_default['items_per_slide']) && $styles_default['items_per_slide'] > 0) {
    $items_per_slide = $styles_default['items_per_slide'];
  }
  else {
    $items_per_slide = 1;
  }

  $vars['items_per_slide'] = $items_per_slide;

  $items = array();
  $slideshow_count = 0;
  $rendered_rows = '';
  foreach ($rows as $count => $item) {
    $items[] = $item;
    if (count($items) == $items_per_slide || $count == (count($rows)-1)) {
      $rendered_rows .= theme(views_theme_functions('views_slideshow_swiper_main_frame_row', $vars['view'], $vars['view']->display[$vars['view']->current_display]), array('vss_id' => $vss_id, 'items' => $items, 'count' => $slideshow_count, 'view' => $vars['view']));
      $items = array();
      $slideshow_count++;
    }
  }

  $vars['rendered_rows'] = $rendered_rows;
}

/**
 * Views Slideshow slideshow rows.
 *
 * @ingroup themeable
 */
function _views_slideshow_swiper_preprocess_views_slideshow_swiper_main_frame_row(&$vars) {
  $current = $vars['count'] + 1;
  $vars['classes_array'][] = 'views_slideshow_swiper_slide';
  $vars['classes_array'][] = 'views_slideshow_slide views-row-' . $current;

  if ($vars['count']) {
    $vars['classes_array'][] =  'views_slideshow_swiper_hidden';
  }
  $vars['classes_array'][] = ($vars['count'] % 2) ? 'views-row-even' : 'views-row-odd';

  $vars['rendered_items'] = '';
  foreach ($vars['items'] as $item_count => $item) {
    $vars['rendered_items'] .= theme(views_theme_functions('views_slideshow_swiper_main_frame_row_item', $vars['view'], $vars['view']->display[$vars['view']->current_display]), array('item' => $item, 'item_count' => $item_count, 'count' => $vars['count'], 'view' => $vars['view'], 'length' => count($vars['view']->result)));
  }
}

function _views_slideshow_swiper_preprocess_views_slideshow_swiper_main_frame_row_item(&$vars) {
  $vars['classes_array'][] = 'views-row views-row-' . $vars['count'];
  $vars['classes_array'][] = ($vars['count'] % 2) ? 'views-row-even' : 'views-row-odd';
  if ($vars['count'] == 0) {
    $vars['classes_array'][] = 'views-row-first';
  }
  elseif ($vars['count'] == $vars['length'] - 1) {
    $vars['classes_array'][] = 'views-row-last';
  }

  /**
   * Support custom row classes.
   */
  if ($row_class = $vars['view']->style_plugin->get_row_class($vars['count'])) {
    $vars['classes_array'][] = $row_class;
  }
}
