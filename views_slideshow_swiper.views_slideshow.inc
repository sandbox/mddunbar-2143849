<?php

/**
 * @file
 * The default options available with Views Slideshow: swiper.
 */

/**
 * Implements hook_views_slideshow_slideshow_info().
 */
function views_slideshow_swiper_views_slideshow_slideshow_info() {
  $options = array(
    'views_slideshow_swiper' => array(
      'name' => t('Swiper'),
      'accepts' => array(
        'goToSlide',
        'nextSlide',
        'pause',
        'play',
        'previousSlide',
      ),
      'calls' => array(
        'transitionBegin',
        'transitionEnd',
        'goToSlide',
        'pause',
        'play',
        'nextSlide',
        'previousSlide',
      ),
    ),
  );
  return $options;
}

/**
 * Implements hook_views_slideshow_option_definition().
 */
function views_slideshow_swiper_views_slideshow_option_definition() {
  $options['views_slideshow_swiper'] = array(
    'contains' => array(
      'display_mode' => array('default' => 'horizontal'),
      'slide_duration' => array('default' => 5000),
      'slides_per_view' => array('default' => 1),
      'slides_per_group' => array('default' => 1),
      'resistance' => array('default' => true),
  /*    // Transition
      'effect' => array('default' => 'fade'),
      'transition_advanced' => array('default' => 0),
      'timeout' => array('default' => 5000),
      'speed' => array('default' => 700), //normal
      'delay' => array('default' => 0),
      'sync' => array('default' => 1),
      'random' => array('default' => 0),

      // Action
      'pause' => array('default' => 1),
      'pause_on_click' => array('default' => 0),
      'action_advanced' => array('default' => 0),
      'start_paused' => array('default', 0),
      'remember_slide' => array('default' => 0),
      'remember_slide_days' => array('default' => 1),
      'pause_in_middle' => array('default' => 0),
      'pause_when_hidden' => array('default' => 0),
      'pause_when_hidden_type' => array('default' => 'full'),
      'amount_allowed_visible' => array('default' => ''),
      'nowrap' => array('default' => 0),
      'fixed_height' => array('default' => 1),
      'items_per_slide' => array('default' => 1),
      'wait_for_image_load' => array('default' => 1),
      'wait_for_image_load_timeout' => array('default' => 3000),

      // Internet Explorer Tweaks
      'cleartype' => array('default' => 'true'),
      'cleartypenobg' => array('default' => 'false'),

      // Advanced
      'advanced_options' => array('default' => '{}'),*/
    ),
  );
  return $options;
}

/**
 * Implements hook_views_slideshow_slideshow_slideshow_type_form().
 */
function views_slideshow_swiper_views_slideshow_slideshow_type_form(&$form, &$form_state, &$view) {
  if (!$swiper_path = _views_slideshow_swiper_library_path()) {
    $form['views_slideshow_swiper']['no_swiper_js'] = array(
      '#markup' => '<div style="color: red">' . t('You need to install the jQuery swiper plugin. Create a directory in sites/all/libraries called jquery.swiper, and then copy jquery.swiper.all.min.js or jquery.swiper.all.js into it. You can find the plugin at !url.', array('!url' => l('http://malsup.com/jquery/swiper', 'http://malsup.com/jquery/swiper', array('attributes' => array('target' => '_blank'))))) . '</div>',
    );
  }
  
  $form['views_slideshow_swiper']['display_mode'] = array(
    '#type' => 'select',
    '#options' => array(
      'horizontal' => 'Horizontal',
      'vertical' => 'Vertical',
    ),
    '#title' => t('Display Mode'),
    '#default_value' => $view->options['views_slideshow_swiper']['display_mode'],
    '#description' => t('The display mode of the slider.'),
  );
    
  $form['views_slideshow_swiper']['slide_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Duration'),
    '#default_value' => $view->options['views_slideshow_swiper']['slide_duration'],
    '#description' => t('The duration of each slide'),
  );

  $form['views_slideshow_swiper']['slides_per_view'] = array(
    '#type' => 'textfield',
    '#title' => t('Slides Per View'),
    '#default_value' => $view->options['views_slideshow_swiper']['slides_per_view'],
    '#description' => t('The number of slides displayed at one time.'),
  );
    
  $form['views_slideshow_swiper']['slides_per_group'] = array(
    '#type' => 'textfield',
    '#title' => t('Slides Per Group'),
    '#default_value' => $view->options['views_slideshow_swiper']['slides_per_group'],
    '#description' => t('The number of slides that will be grouped for sliding together.'),
  );
  
  $form['views_slideshow_swiper']['resistance'] = array(
    '#type' => 'select',
    '#options' => array(
      true => 'Include Resistence',
      false => 'Do Not Include Resistence',
      '100%' => 'Do Not Allow Scroll Past',
    ),
    '#title' => t('Resistence'),
    '#default_value' => $view->options['views_slideshow_swiper']['resistance'],
    '#description' => t('How the slider should behave when sliding past the last slide item.'),
  );
  
  
}

function views_slideshow_swiper_form_options_js($element, &$form_state, &$form) {
  ctools_add_js('formoptions', 'views_slideshow_swiper');
  return $element;
}

/**
 * Implements hook_views_slideshow_options_form_validate().
 */
function views_slideshow_swiper_views_slideshow_options_form_validate(&$form, &$form_state, &$view) {
  if (!is_numeric($form_state['values']['style_options']['views_slideshow_swiper']['speed'])) {
    form_error($form['views_slideshow_swiper']['speed'], t('!setting must be numeric!', array('Speed')));
  }
  if (!is_numeric($form_state['values']['style_options']['views_slideshow_swiper']['timeout'])) {
    form_error($form['views_slideshow_swiper']['speed'], t('!setting must be numeric!', array('timeout')));
  }
  if (!is_numeric($form_state['values']['style_options']['views_slideshow_swiper']['remember_slide_days'])) {
    form_error($form['views_slideshow_swiper']['remember_slide_days'], t('!setting must be numeric!', array('Slide days')));
  }
}
